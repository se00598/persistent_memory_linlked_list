#include "include/heap.h"
#include "include/ring_buffer.h"
#include "include/recovery.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <malloc.h>
#include <bits/pthreadtypes.h>
#include <pthread.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
//#define DEBUG 1
#include "include/common.h"
#include "LinkedList.h"



struct server_event_loop_thread_context{
    int port_number;
    PMEMobjpool *pop;
    struct ring_buffer *rb;
};
/* ------------------------------------- */

/**
 * Creates an empty linkedlist_t. head is set to NULL and length to 0.
 */
linkedlist_t *linkedlist_init(heap_t *heap) {
    linkedlist_t *list = heap_alloc(heap, sizeof(linkedlist_t));
    if(list == NULL) return NULL;
    list->length = 0;
    list->head = NULL;
    return list;
}

/**
 * Adds a value at the end of the given list.
 */
void linkedlist_add(heap_t *heap, linkedlist_t * list, void *value) {
    list_node_t * node = linkedlist_create_node(heap, value, NULL);
    if(list->length) {
        list_node_t * last = linkedlist_get_node(list, list->length-1);
        last->next = linkedlist_create_node(heap, value,NULL);
    } else {
        list->head = node;
    }
    list->length++;
}

/**
 * Creates a node with the given nullable value and returns the result.
 * Should be freed on removal from the list.
 */
list_node_t * linkedlist_create_node(heap_t *heap, void *value, list_node_t * next) {
    list_node_t * node = NULL;
    node = heap_alloc(heap, sizeof(list_node_t));
    if(node == NULL) {
        perror("Node is null, exiting\n");
        exit(1);
    }
    node->val = value;
    node->next = next;
    return node;
}

/**
 * Removes and frees the node at the given index
 */
void linkedlist_remove_index(linkedlist_t * list, int index) {

    if(list->length == 0) return;

    if(!index) {
        linkedlist_pop(list);
        return;
    }

    list_node_t * current = list->head;
    list_node_t * temp_node = NULL;
    for(int i = 1; i < index && current != NULL ; i++) {
        current = current->next;
    }

    temp_node = current->next;
    current->next = temp_node->next;
    if(temp_node->val != NULL)
        free(temp_node->val);
    free(temp_node);

    list->length--;
}

/**
 * Removes and frees the first node in the list
 */
void linkedlist_pop(linkedlist_t * list) {
    if(!list->length) return;
    list_node_t * next_node = NULL;
    list_node_t ** head = &list->head;

    next_node = (*head)->next;
    if((*head)->val != NULL)
        free((*head)->val);
    free(*head);
    *head = next_node;
    list->length--;
}

/**
 * Returns the value of the node at the given index
 */
void *linkedlist_get(linkedlist_t * list, int index) {
    list_node_t *node = linkedlist_get_node(list,index);
    return node != NULL ? node->val : NULL;
}

/**
 * Returns the node at the given index
 */
list_node_t * linkedlist_get_node(linkedlist_t * list, int index) {
    if(index >= list->length || index < 0) return NULL;
    list_node_t * current = list->head;
    for(int i = 1; i <= index && current != NULL ; i++) {
        current = current->next;
    }
    return current;
}

/**
 * Inserts a newly created node from the given value at the given index
 */
void linkedlist_insert(heap_t *heap, linkedlist_t * list, int index, void *value) {
    if(index == list->length) {
        linkedlist_add(heap, list,value);
    } else if(!index) {
        linkedlist_push(heap,list,value);
    } else {
        list_node_t * previous = linkedlist_get_node(list, index-1);
        previous->next = linkedlist_create_node(heap, value,previous->next);

        list->length++;
    }
}

/**
 * Adds a newly created node from the given value at the start of the list
 */
void linkedlist_push(heap_t *heap, linkedlist_t * list, void *value) {
    list_node_t ** head = &list->head;
    list_node_t * newNode = linkedlist_create_node(heap,value,*head);
    *head = newNode;
    list->length++;
}

/**
 * Safe free of the list. Also frees the values.
 */
void linkedlist_free(linkedlist_t * list) {
    if(list == NULL) return;
    list_node_t * current;
    while ((current = list->head) != NULL) {
        list->head = list->head->next;
        if(current->val != NULL)
            free (current->val);
        free (current);
    }
    free(list);
}

/* ------------------------------------- */


void runApplicationInitProcedure(struct application_data *applicationData, heap_t *heap){
    /* Set application values here using application_data struct */
    /* Use heap_alloc for memory allocations */
    applicationData->linkedlist = linkedlist_init(heap);
}

bool isUpdateOperation(char *cmd){
    /* Define code that returns true if an operation contains an update
     * and false otherwise */
    if(strcmp(cmd, "insert") == 0){
        return true;
    }
    return false;
}
/* ------------------------------------- */


void proder_loop(PMEMobjpool *pop, struct ring_buffer *ringBuffer, heap_t *heap, struct linkedlist *ll){
    /* App specific: Get request from user */
    char command[10];
    for(;;) {
        bzero(command, strlen(command));
        printf("Type the command:\n");

        fgets(command,10,stdin);
        command[strcspn(command, "\r\n")] = 0;
        if(strcmp(command, "exit") == 0){
            printf("Exiting\n");
            exit(0);
        } else if(strcmp(command, "print") == 0){
            printf("Length: %d\n", ll->length);
            printf("Elements: \n");
            for(int i=0; i< ll->length; i++){
                printf("%s\n", (char*)linkedlist_get_node(ll, i)->val);
            }
        }

        if (isUpdateOperation(command)) {
            /* If so, log */
            while (log_to_pmem(pop, ringBuffer, command, 1) != 0) {
                /* Keep trying to log */
            }
            /* Execute */
            if(strcmp(command, "insert") == 0){
                linkedlist_insert(heap, ll,0, "node_payload");
            }
        }
    }
}

void startStateMachine(){

}

void server_event_loop(PMEMobjpool *pop, struct ring_buffer *rb, int connection_file_descriptor){
    char msg_buffer[MESSAGE_BUFFER_SIZE];
    char command_request[CMD_REQUEST_BUFFER_SIZE];
    DEBUG_PRINT(printf("Started server event loop\n"));


    for(;;){
        bzero(command_request, CMD_REQUEST_BUFFER_SIZE);
        read(connection_file_descriptor, command_request, CMD_REQUEST_BUFFER_SIZE);


        int index = strtoll(command_request, NULL, 10);


        DEBUG_PRINT(("Received from client: %d\n", index));
        if(strcmp("e", msg_buffer) == 0){
            DEBUG_PRINT(("Received exit command\n"));
            close(connection_file_descriptor);
            break;
        }
        bzero(msg_buffer, MESSAGE_BUFFER_SIZE);
        //char *server_response_message = "1\n";
        /* struct cmd and next index*/
        struct  cmd_and_next_index *cmdAndNextIndex= retrieve_command_at_index(pop, rb, index);
        DEBUG_PRINT(("Server command size: %d, command %s\n", cmdAndNextIndex->command_size, cmdAndNextIndex->command));
        if(cmdAndNextIndex->next_index >= 0){
            struct server_response serverResponse;
            serverResponse.nextCommandIndex = cmdAndNextIndex->next_index;
            memcpy(serverResponse.command, cmdAndNextIndex->command, cmdAndNextIndex->command_size);
            write(connection_file_descriptor, &serverResponse, sizeof (struct server_response));
        } else{
            struct server_response serverResponse;
            serverResponse.nextCommandIndex = -1;
            char *emptyLog = "NULL";
            memcpy(serverResponse.command, emptyLog, strlen(emptyLog));
            write(connection_file_descriptor, &serverResponse, sizeof (struct server_response));
        }


    }
    DEBUG_PRINT(("Post loop\n"));
    close(connection_file_descriptor);
}

void *start_server_thread(void *context){
    // Start in a new thread
    struct server_event_loop_thread_context *tc = context;
    int server_port_number = tc->port_number;
    int socket_desc, connection_file_descriptor, c;
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    DEBUG_PRINT(("Created socket\n"));
    if(socket_desc == -1){
        printf("Could not create socket\n");
        exit(-1);
    }

    struct sockaddr_in server, client;
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(server_port_number);

    if(bind(socket_desc, (struct sockaddr *)&server, sizeof (server)) < 0){
        perror("Bind failed");
        exit(1);
    }
    DEBUG_PRINT(("Bind is done"));
    listen(socket_desc, 3);

    DEBUG_PRINT(("Waiting for incoming connections.."));
    c = sizeof (struct sockaddr_in);

    connection_file_descriptor = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (connection_file_descriptor < 0) {
        perror("Accept failed");
        exit(1);
    }
    DEBUG_PRINT(("Starting a chat\n"));

    server_event_loop(tc->pop, tc->rb, connection_file_descriptor);

    return NULL;
}

void execute_command(struct metaData *metaData, char *command){
    DEBUG_PRINT(("Command[0] is %c\n", command[0]));
    if(strcmp(command, "insert") == 0){
        //DEBUG_PRINT(("Pushing %c to the list\n", command[1]));
        void *ll_entry = heap_alloc(metaData->heap,10);
        memcpy(ll_entry, "node_payload", 12);

        linkedlist_push(metaData->heap, metaData->app_data.linkedlist, ll_entry);
    }
}

_Noreturn void startConsumerServer(int psm_port_number, struct metaData *metaData){
    /* Init connection */
    printf("Link list size: %d\n", metaData->app_data.linkedlist->length);

    int psm_sock_descriptor;
    struct sockaddr_in pmsm_server;
    psm_sock_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if(psm_sock_descriptor == -1){
        DEBUG_PRINT(("Could not create socket\n"));
        exit(-1);
    }
    pmsm_server.sin_addr.s_addr = inet_addr("127.0.0.1");
    pmsm_server.sin_family = AF_INET;
    pmsm_server.sin_port = htons(psm_port_number);

    if(connect(psm_sock_descriptor, (struct sockaddr *)&pmsm_server, sizeof(pmsm_server)) < 0){
        perror("Connection error");
        exit(1);
    }
    /* Request nextCommand index from server */
    /* TODO make sure this does not trigger faults*/

    //char msg_buffer[MESSAGE_BUFFER_SIZE];
    struct server_response serverResponse;
    //bzero(msg_buffer, MESSAGE_BUFFER_SIZE);
    char command_request[CMD_REQUEST_BUFFER_SIZE];

    for(;;) {

        bzero(command_request, CMD_REQUEST_BUFFER_SIZE);
        int i = metaData->nextCommandIndex;
        sprintf(command_request, "%d", i);
        write(psm_sock_descriptor, command_request, CMD_REQUEST_BUFFER_SIZE);
        DEBUG_PRINT(("Sent request for command %s\n", command_request));
        bzero(&serverResponse, sizeof (struct server_response));

        read(psm_sock_descriptor, &serverResponse, sizeof (struct server_response));
        DEBUG_PRINT(("Received: %d, command %s\n", serverResponse.nextCommandIndex, serverResponse.command));


        if(serverResponse.nextCommandIndex >= 0 ){
            sm_op_begin();
            execute_command(metaData, serverResponse.command);
            DEBUG_PRINT(("Link list size: %d\n", metaData->app_data.linkedlist->length));
            /* Call execute command from here for example
             * executeCommand(serverResponse.command);
            */
            DEBUG_PRINT(("TODO: execute this command\n"));
            /* if success start transaction
             * execute command
             * update nextCommand index
             * commit
             */
            metaData->nextCommandIndex = serverResponse.nextCommandIndex;
            sm_op_end();

        }

    }

}

void run_integrated_test(PMEMobjpool *pop, struct metaData *metaData, struct ring_buffer *rb){
    clock_t begin = clock();
    clock_t begin_consumption = clock();
    int op_size = 500000;
    for(int i=0; i< op_size; i++){
        char el[2];
        bzero(el, 2);
        el[0] = 'a';
        void *ll_entry = heap_alloc(metaData->heap,10);
        memcpy(ll_entry, "a", 2);
        char log_entry[2];
        bzero(el, 2);
        log_entry[0] = 'p';
        log_entry[1] = 'a';
        log_to_pmem(pop, rb, log_entry, 100);
        linkedlist_push(metaData->heap, metaData->app_data.linkedlist, ll_entry);
    }
    clock_t end = clock();
    double push_and_log = (double)(end - begin) / CLOCKS_PER_SEC;

    list_node_t *n = linkedlist_get_node(metaData->app_data.linkedlist, 0);
    char *res = n->val;
    //printf("Got \" %s \" node from the list\n", res);
    //printf("Link list size: %d\n", metaData->app_data.linkedlist->length);
    //while(rb->number_of_commands > 0){
        //printf("Waiting for log to be processed\n");

    //}
    clock_t consumption_end = clock();
    printf("Took: %f seconds to push %d elements\n", push_and_log, op_size);
    double push_log_consume = (double)(consumption_end - begin_consumption) / CLOCKS_PER_SEC;
    printf("Took: %f seconds to consume %d elements\n", push_log_consume, op_size);
    printf("Log has been cleared\n");
}

int main(int argc, const char *argv[]) {

    struct memoryAgnosticRoot *pmemRoot;
    if(strcmp(argv[1], "dram") == 0){
        DEBUG_PRINT(("Debug message!"));
        //Open pmem pool
        //
        struct metaData *metaData;
        if(strcmp(argv[2], "recover") == 0){
            //struct memoryAgnosticRoot *dramRoot;
            struct memoryAgnosticRoot *memoryAgnosticRoot = recoverMetaData(memoryAgnosticRoot);
            //metaData = runInitProcedure(memoryAgnosticRoot,false);
            DEBUG_PRINT(("In the main method\n"));
            DEBUG_PRINT(("Heap addr from the main: %p\n", memoryAgnosticRoot->metaData.heap));
            metaData = runInitProcedure(memoryAgnosticRoot,false, false);
            DEBUG_PRINT(("Malloc returns address: %p\n", heap_alloc(memoryAgnosticRoot->metaData.heap, 10)));
            DEBUG_PRINT(("Metadata malloc returns address: %p\n", heap_alloc(metaData->heap, 10)));
            metaData = &memoryAgnosticRoot->metaData;

            int length = metaData->app_data.linkedlist->length;
            for(int i=0; i< length; i++){
                list_node_t *n =linkedlist_get_node(metaData->app_data.linkedlist, i);
                DEBUG_PRINT(("Getting elements from the list\n"));
                if(n == NULL){
                    DEBUG_PRINT(("N is null\n"));
                }
                if(metaData->app_data.linkedlist == NULL){
                    DEBUG_PRINT(("Linked list is null\n"));
                } else{
                    DEBUG_PRINT(("Link list address is: %p\n", metaData->app_data.linkedlist));
                    DEBUG_PRINT(("Link list size: %d\n", metaData->app_data.linkedlist->length));
                }
                char *res = n->val;
                DEBUG_PRINT(("Got \" %s \" node from the list\n", res));
            }

        } else if(strcmp(argv[2], "init") == 0){
            DEBUG_PRINT(("DEBUG MESSAGE"));
            metaData = runInitProcedure(pmemRoot, true, true);
            runApplicationInitProcedure(&metaData->app_data, metaData->heap);

            DEBUG_PRINT(("Malloc returns address: %p\n", heap_alloc(metaData->heap, 10)));
        } else if(strcmp(argv[2], "catch_up") == 0){
            DEBUG_PRINT(("Catching up\n"));
        }else{
            DEBUG_PRINT(("Need to choose correct option\n"));
            exit(1);
        }
        /* ---- Connect to the log------ */
        PMEMobjpool *pop = NULL;
        pop = mmap_pmem_object_pool_ring_buffer(pop);
        struct ring_buffer *rb = initialise_ring_buffer(pop);
        DEBUG_PRINT(("Back in the main\n"));

        /* ---- Init server thread ----- */
        struct server_event_loop_thread_context *tc = malloc(sizeof (struct server_event_loop_thread_context));
        tc->port_number = atoi(argv[3]);
        tc->pop = pop;
        tc->rb = rb;
        pthread_t server_thread_obj;




        pthread_create( &server_thread_obj, NULL, start_server_thread, tc);
        DEBUG_PRINT(("initialised server thread\n"));
        /* ---- Start state machine------ */





        //printf("----------Connection test for the consumer-----------");
        /*
        char el[2];
        bzero(el, 2);
        el[0] = 'a';
        void *ll_entry = heap_alloc(metaData->heap,10);
        memcpy(ll_entry, "a", 2);
        char log_entry[2];
        bzero(el, 2);
        log_entry[0] = 'p';
        log_entry[1] = 'a';
        log_to_pmem(pop, rb, log_entry);
        linkedlist_push(metaData->heap, metaData->app_data.linkedlist, ll_entry);
        //while(rb->number_of_commands > 0){
        //Replace this with head == tail
            //printf("Waiting for log to be processed\n");

        //}
         */
        //printf("------------ Real test ---------------------\n");

        run_integrated_test(pop, metaData, rb);

        //printf("About to exit\n");


        //exit(0);
        /*
        log_to_pmem(pop, rb, "first command");
        log_to_pmem(pop, rb, "second command");
        log_to_pmem(pop, rb, "third command");
         */

        proder_loop(pop, rb, metaData->heap, metaData->app_data.linkedlist);


    } else if(strcmp(argv[1], "pmem") == 0){
        struct metaData *metaData;
        if(strcmp(argv[2], "recover") == 0){
            metaData = runInitProcedure(pmemRoot,false, true);
            startConsumerServer(atoi(argv[3]), metaData);
        } else if(strcmp(argv[2], "init") == 0){
            metaData = runInitProcedure(pmemRoot,true, true);
            /* Application specific procedure call */
            runApplicationInitProcedure(&metaData->app_data, metaData->heap);
            DEBUG_PRINT(("Link list address is: %p\n", metaData->app_data.linkedlist));
            startConsumerServer(atoi(argv[3]), metaData);
        } else if(strcmp(argv[2], "instrumet_performance_test") == 0){
            metaData = runInitProcedure(pmemRoot,true, true);
            /* Application specific procedure call */
            runApplicationInitProcedure(&metaData->app_data, metaData->heap);
            DEBUG_PRINT(("Link list address is: %p\n", metaData->app_data.linkedlist));
            //time;
            DEBUG_PRINT(("Debug message!"));
            sm_op_begin();
            clock_t begin = clock();

            for(int i = 1; i<50000; i++){
                if(i % 10000 == 0){
                    //printf("Broke down on: %d\n", i);
                    //exit(1);
                }
                linkedlist_push(metaData->heap, metaData->app_data.linkedlist, "foobar");
            }

            clock_t end = clock();
            sm_op_end();
            double push_and_log = (double)(end - begin) / CLOCKS_PER_SEC;
            printf("Took: %f seconds to push %d elements\n", push_and_log, 500000);
    }

    else if(strcmp(argv[2], "test_case") == 0){
            metaData = runInitProcedure(pmemRoot,true, true);
            run_graceful_exit();
        }else if(strcmp(argv[2], "close_file_correctly") == 0){
            //metaData = runInitProcedure(pmemRoot,true, true);
            run_graceful_exit();
        }
        //printf("Malloc returns address: %p\n", heap_alloc(metaData->heap, 10));
        //run_graceful_exit();

        /* ---- Start consumer server ----- */


    }

    return 0;
}
