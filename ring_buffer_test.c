#include <stdio.h>
#include <bits/types/clock_t.h>
#include <time.h>
#include "include/ring_buffer.h"
#include "include/recovery.h"
#include <string.h>
#include <stdlib.h>
#include <libpmemobj/pool_base.h>
#include <libpmem.h>
#include <libpmemobj/types.h>

static FILE *pFile2;
void run_test(PMEMobjpool *pop, struct ring_buffer *rb, long flush_period){
    reset_ring_buffer(pop, rb);
    clock_t begin = clock();
    for(int i = 0; i< 10000000; i++){
        if (log_to_pmem(pop, rb, "foobar", flush_period) == 1){
            printf("Only %d entries inserted\n", i);
            exit(0);
        }

    }
    clock_t end = clock();
    double log = (double)(end - begin) / CLOCKS_PER_SEC;
    printf(" %f %ld \n", log, flush_period);
}

void run_test_pmem_clean(long flush_period) {
    remove("/mnt/dax/test_outputs/pmemlibfile");
    char *pmemaddr;
    size_t mapped_len;
    int is_pmem;
    if ((pmemaddr = pmem_map_file("/mnt/dax/test_outputs/pmemlibfile", 10000000*20,
                                  PMEM_FILE_CREATE|PMEM_FILE_EXCL,
                                  0666, &mapped_len, &is_pmem)) == NULL) {
        perror("pmem_map_file");
        exit(1);
    }

    void *last_flush = pmemaddr;
    void *temp_ptr = last_flush;
    int command_count =0;
    char *command = "foobar";
    int command_lenght;

    clock_t begin = clock();


    for (int i = 0; i < 10000000; i++) {
        command_lenght = strlen(command);

        memcpy(temp_ptr, command, command_lenght);
        temp_ptr+=command_lenght;
        memcpy(temp_ptr, &command_lenght, 8);
        temp_ptr+=8;
        command_count++;
        if (command_count % flush_period == 0) {
            long distance = temp_ptr - last_flush;
            /* flush it */
            if (pmem_msync(last_flush, distance) < 0) {
                perror("pmem_msync");
                exit(1);
            }
            last_flush = temp_ptr;
            command_count = 0;
        }


    }
    clock_t end = clock();
    double log = (double) (end - begin) / CLOCKS_PER_SEC;
    printf(" %f %ld \n", log, flush_period);

}

void run_append_test(long flush_period){
    int command_count =0;
    int operations = 10000000;

    char dest[16];
    int cmd_size;

    clock_t start = clock();
    for(int i =0; i< operations; i++){
        cmd_size= strlen("foobar");
        memcpy(dest, "foobar", 8);
        memcpy(dest+8, &cmd_size, 8);
        fprintf(pFile2, "%s", dest);
        command_count++;
        if(command_count % flush_period == 0){
            fflush(pFile2);
            command_count = 0;
            //fclose(pFile2);
            //pFile2=fopen("test_file.txt", "a");
        }

    }
    clock_t end = clock();
    double log = (double)(end - start) / CLOCKS_PER_SEC;
    printf(" %f %ld \n", log, flush_period);
}

PMEMobjpool *open_file(PMEMobjpool *pop){
    char *path_to_pmem = "/mnt/dax/test_outputs/log_test_straignt_pmem";
    if (file_exists((path_to_pmem)) != 0) {
        if ((pop = pmemobj_create(path_to_pmem, POBJ_LAYOUT_NAME(list),PMEMOBJ_MIN_POOL*1000, 0666)) == NULL)
            perror("failed to create pool\n");
    } else {
        if ((pop = pmemobj_open(path_to_pmem, POBJ_LAYOUT_NAME(list))) == NULL) {
            perror("failed to open pool\n");
        }
    }
    return pop;
}

void test_with_pmempbj(int flush_period){
    PMEMobjpool *pop;
    pop = open_file(pop);



    PMEMoid pool_root = pmemobj_root(pop, sizeof(struct pmem_obj_root));

    struct pmem_obj_root *rootp = pmemobj_direct(pool_root);
    void *pmem = rootp->rb.buffer;

    char *command = "foobar";
    int command_lenght;
    clock_t start = clock();
    void *temp_ptr = pmem;
    int command_count = 0;

    for (int i = 0; i < 10000000; i++) {
        command_lenght = strlen(command);
        memcpy(temp_ptr, command, command_lenght);
        temp_ptr+=command_lenght;
        command_count++;
        if (command_count % flush_period == 0) {
            long distance = temp_ptr - pmem;
            /* flush it */
            pmemobj_persist(pop, pmem, distance);
            pmem = temp_ptr;
            command_count = 0;
            pmemobj_close(pop);
            pop = open_file(pop);
        }

    }
    clock_t end = clock();
    double log = (double) (end - start) / CLOCKS_PER_SEC;
    printf(" %f %d \n", log, flush_period);
    pmemobj_close(pop);
}
/*
int main(){
    PMEMobjpool *pop = NULL;
    pop = mmap_pmem_object_pool_ring_buffer(pop);
    struct ring_buffer *rb = initialise_ring_buffer(pop);
    printf("Ring buffer\n");
    run_test(pop, rb, 10);
    run_test(pop, rb, 100);
    run_test(pop, rb, 1000);
    run_test(pop, rb, 10000);
    run_test(pop, rb, 100000);
    run_test(pop, rb, 1000000);
    run_test(pop, rb, 10000000);

    printf("Clean pmem\n");
    //run_test_pmem_clean(1000);
    run_test_pmem_clean(10000);
    run_test_pmem_clean(100000);
    run_test_pmem_clean(1000000);
    run_test_pmem_clean(10000000);

    printf("Append file\n");

    pFile2=fopen("test_file.txt", "a");
    run_append_test( 10);
    run_append_test( 100);
    run_append_test( 1000);
    run_append_test( 10000);
    run_append_test(100000);
    run_append_test( 1000000);
    run_append_test(10000000);
    fclose(pFile2);
    printf("Pmemobj\n");

    test_with_pmempbj(100000);
    test_with_pmempbj(1000000);
    test_with_pmempbj(10000000);

}
 */

