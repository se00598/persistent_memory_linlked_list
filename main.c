#include "include/heap.h"
#include "include/ring_buffer.h"
#include "include/recovery.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <malloc.h>
#include <bits/pthreadtypes.h>
#include <pthread.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "include/common.h"


struct server_event_loop_thread_context{
    int port_number;
    PMEMobjpool *pop;
    struct ring_buffer *rb;
};
/* ------------------------------------- */
struct application_data{
    /* Insert application root pointers here */
};

void runApplicationInitProcedure(struct application_data *applicationData, heap_t *heap){
    /* Set application values here using application_data struct */
    /* Use heap_alloc for memory allocations */
}

bool isUpdateOperation(char *cmd){
    /* Define code that returns true if an operation contains an update
     * and false otherwise */
    return false;
}
/* ------------------------------------- */

void state_machine_loop(PMEMobjpool *pop, struct ring_buffer *ringBuffer){
    /* App specific: Get request from user */
    for(;;) {
        char *cmd = "This should be replaced with a client command";
        /* App specific: Check if it is an update operation */
        printf("In the state machine loop\n");
        sleep(1);
        if (isUpdateOperation(cmd)) {
            /* If so, log */
            while (log_to_pmem(pop, ringBuffer, cmd) != 0) {
                /* Keep trying to log */
            }
            /* Execute */
        }
    }
}

void startStateMachine(){

}

void server_event_loop(PMEMobjpool *pop, struct ring_buffer *rb, int connection_file_descriptor){
    char msg_buffer[MESSAGE_BUFFER_SIZE];
    char command_request[CMD_REQUEST_BUFFER_SIZE];
    printf("Started server event loop\n");


    for(;;){
        bzero(command_request, CMD_REQUEST_BUFFER_SIZE);
        read(connection_file_descriptor, command_request, CMD_REQUEST_BUFFER_SIZE);
        int index = strtoll(command_request, NULL, 10);


        printf("Received from client: %d\n", index);
        if(strcmp("e", msg_buffer) == 0){
            printf("Received exit command\n");
            close(connection_file_descriptor);
            break;
        }
        bzero(msg_buffer, MESSAGE_BUFFER_SIZE);
        //char *server_response_message = "1\n";
        /* struct cmd and next index*/
        struct  cmd_and_next_index *cmdAndNextIndex= retrieve_command_at_index(pop, rb, atoi(command_request));
        printf("Server command size: %d, command %s\n", cmdAndNextIndex->command_size, cmdAndNextIndex->command);
        if(cmdAndNextIndex->next_index >= 0){
            struct server_response serverResponse;
            serverResponse.nextCommandIndex = cmdAndNextIndex->next_index;
            memcpy(serverResponse.command, cmdAndNextIndex->command, cmdAndNextIndex->command_size);
            write(connection_file_descriptor, &serverResponse, sizeof (struct server_response));
        } else{
            struct server_response serverResponse;
            serverResponse.nextCommandIndex = -1;
            char *emptyLog = "NULL";
            memcpy(serverResponse.command, emptyLog, strlen(emptyLog));
            write(connection_file_descriptor, &serverResponse, sizeof (struct server_response));
        }


    }
    printf("Post loop\n");
    close(connection_file_descriptor);
}

void *start_server_thread(void *context){
    // Start in a new thread
    struct server_event_loop_thread_context *tc = context;
    int server_port_number = tc->port_number;
    int socket_desc, connection_file_descriptor, c;
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    printf("Created socket\n");
    if(socket_desc == -1){
        printf("Could not create socket\n");
        exit(-1);
    }

    struct sockaddr_in server, client;
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(server_port_number);

    if(bind(socket_desc, (struct sockaddr *)&server, sizeof (server)) < 0){
        perror("Bind failed");
        exit(1);
    }
    puts("Bind is done");
    listen(socket_desc, 3);

    puts("Waiting for incoming connections..");
    c = sizeof (struct sockaddr_in);

    connection_file_descriptor = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (connection_file_descriptor < 0) {
        perror("Accept failed");
        exit(1);
    }
    printf("Starting a chat\n");

    server_event_loop(tc->pop, tc->rb, connection_file_descriptor);

    return NULL;
}


_Noreturn void startConsumerServer(int psm_port_number, struct metaData *metaData){
    /* Init connection */
    int psm_sock_descriptor;
    struct sockaddr_in pmsm_server;
    psm_sock_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if(psm_sock_descriptor == -1){
        printf("Could not create socket\n");
        exit(-1);
    }
    pmsm_server.sin_addr.s_addr = inet_addr("127.0.0.1");
    pmsm_server.sin_family = AF_INET;
    pmsm_server.sin_port = htons(psm_port_number);

    if(connect(psm_sock_descriptor, (struct sockaddr *)&pmsm_server, sizeof(pmsm_server)) < 0){
        perror("Connection error");
        exit(1);
    }
    /* Request nextCommand index from server */
    /* TODO make sure this does not trigger faults*/

    //char msg_buffer[MESSAGE_BUFFER_SIZE];
    struct server_response serverResponse;
    //bzero(msg_buffer, MESSAGE_BUFFER_SIZE);
    char command_request[CMD_REQUEST_BUFFER_SIZE];


    for(;;) {

        bzero(command_request, CMD_REQUEST_BUFFER_SIZE);
        int i = metaData->nextCommandIndex;
        sprintf(command_request, "%d", i);
        write(psm_sock_descriptor, command_request, CMD_REQUEST_BUFFER_SIZE);
        printf("Sent request for command %s\n", command_request);
        bzero(&serverResponse, sizeof (struct server_response));

        read(psm_sock_descriptor, &serverResponse, sizeof (struct server_response));
        printf("Received: %d, command %s\n", serverResponse.nextCommandIndex, serverResponse.command);

        if(serverResponse.nextCommandIndex >= 0 ){
            sm_op_begin();

            /* Call execute command from here for example
             * executeCommand(serverResponse.command);
            */
            printf("TODO: execute this command\n");
            /* if success start transaction
             * execute command
             * update nextCommand index
             * commit
             */
            metaData->nextCommandIndex = serverResponse.nextCommandIndex;
            sm_op_end();
        }

        /* Else, request again */
        sleep(1);
    }
}

int main(int argc, const char *argv[]) {
    struct memoryAgnosticRoot *pmemRoot;
    if(strcmp(argv[1], "dram") == 0){
        //Open pmem pool
        //

        if(strcmp(argv[2], "recover") == 0){
            //struct memoryAgnosticRoot *dramRoot;
            struct memoryAgnosticRoot *memoryAgnosticRoot = recoverMetaData(memoryAgnosticRoot);
            //metaData = runInitProcedure(memoryAgnosticRoot,false);
            printf("In the main method\n");
            printf("Heap addr from the main: %p\n", memoryAgnosticRoot->metaData.heap);
            //struct metaData *metaData = runInitProcedure(memoryAgnosticRoot,false, false);
            printf("Malloc returns address: %p\n", heap_alloc(memoryAgnosticRoot->metaData.heap, 10));
        } else if(strcmp(argv[2], "init") == 0){
            struct metaData *metaData = runInitProcedure(pmemRoot, true, true);
            runApplicationInitProcedure(metaData->app_data, metaData->heap);
            printf("Malloc returns address: %p\n", heap_alloc(metaData->heap, 10));
        }
        /* ---- Connect to the log------ */
        PMEMobjpool *pop = NULL;
        pop = mmap_pmem_object_pool_ring_buffer(pop);
        struct ring_buffer *rb = initialise_ring_buffer(pop);
        printf("Back in the main\n");

        /* ---- Init server thread ----- */
        struct server_event_loop_thread_context *tc = malloc(sizeof (struct server_event_loop_thread_context));
        tc->port_number = atoi(argv[3]);
        tc->pop = pop;
        tc->rb = rb;
        pthread_t server_thread_obj;
        pthread_create( &server_thread_obj, NULL, start_server_thread, tc);
        printf("initialised server thread\n");
        /* ---- Start state machine------ */
        log_to_pmem(pop, rb, "first command");
        log_to_pmem(pop, rb, "second command");
        log_to_pmem(pop, rb, "third command");
        state_machine_loop(pop, rb);


    } else if(strcmp(argv[1], "pmem") == 0){
        struct metaData *metaData;
        if(strcmp(argv[2], "recover") == 0){
            metaData = runInitProcedure(pmemRoot,false, true);
            startConsumerServer(atoi(argv[3]), metaData);
        } else if(strcmp(argv[2], "init") == 0){
            metaData = runInitProcedure(pmemRoot,true, true);
            /* Application specific procedure call */
            runApplicationInitProcedure(metaData->app_data, metaData->heap);
            startConsumerServer(atoi(argv[3]), metaData);
        } else if(strcmp(argv[2], "test_case") == 0){
            metaData = runInitProcedure(pmemRoot,true, true);
            runApplicationInitProcedure(metaData->app_data, metaData->heap);
            run_graceful_exit();
        }
        //printf("Malloc returns address: %p\n", heap_alloc(metaData->heap, 10));
        //run_graceful_exit();

        /* ---- Start consumer server ----- */


    }

    return 0;
}
