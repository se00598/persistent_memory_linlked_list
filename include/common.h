//
// Created by se00598 on 5/5/22.
//

#ifndef SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_COMMON_H
#define SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_COMMON_H
#define CMD_REQUEST_BUFFER_SIZE 10
#define MESSAGE_BUFFER_SIZE 300

#ifdef DEBUG
# define DEBUG_PRINT(x) printf x
#else
# define DEBUG_PRINT(x) do {} while (0)
#endif

struct server_response{
    int nextCommandIndex;
    char command[250];
};
void sm_op_begin(void);
void sm_op_end(void);
bool file_exists(const char *path);
#endif //SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_COMMON_H
