//
// Created by se00598 on 5/5/22.
//

#ifndef SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_RECOVERY_H
#define SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_RECOVERY_H

#include "heap.h"
#include "../LinkedList.h"
#include <libpmemobj/base.h>
#include <stdbool.h>

struct application_data{
    /* Insert application root pointers here */
    //struct node *head;
    linkedlist_t *linkedlist;
};

struct metaData{
    char msg[10];
    void *startAddr;
    struct memoryAgnosticRoot *marPtr;
    size_t memoryBlockSize;
    heap_t *heap;
    void *region;
    struct application_data app_data;
    int nextCommandIndex;
};
/* This is positioned on rootp->pmem_start */
struct memoryAgnosticRoot{
    char msg[10];
    struct metaData metaData;
    char memory_buff[64000000];
};

struct my_root {
    bool tx_is_running;
    int log_ops;
    int processed_log_entries;
    char log_file_path[200];
    char log_status;
    int recovery_count;
    long offset;
    long malloc_offset;
    bool instrument_malloc;
    char test_msg[10];
    char pmem_start[128000000];
};
void run_graceful_exit();
struct metaData *runInitProcedure(struct memoryAgnosticRoot *memoryAgnosticRoot, bool firstInit, bool do_the_mmap);
bool file_exists(const char *path);
PMEMobjpool *mmap_pmem_object_pool(PMEMobjpool *pop);
void *getDramRootPtr(PMEMobjpool *pop);
struct memoryAgnosticRoot *recoverMetaData(struct memoryAgnosticRoot *dramRoot);
#endif //SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_RECOVERY_H
