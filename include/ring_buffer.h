//
// Created by se00598 on 5/5/22.
//

#ifndef SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_RING_BUFFER_H
#define SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_RING_BUFFER_H


#include <libpmemobj/base.h>
#include <libpmemobj/tx_base.h>
#include <stdbool.h>

typedef struct assert assert;

struct cacheline_struct{
    uint32_t head;
    uint32_t numner_of_commands;
};

struct ring_buffer{
    //unsigned int number_of_commands;
    unsigned int head;
    unsigned int tail;
    unsigned int size;
    unsigned int real_size;
    signed int last_seen_index;
    struct cacheline_struct cls;
    /* TODO make this configurable*/
    char buffer[1000000000];
};



struct pmem_obj_root{
    struct ring_buffer rb;
};

struct varied_length_command_info{
    int command_lenght;
};
/* TODO */
struct cmd_and_next_index{
    char *command;
    int next_index;
    int command_size;
};

static void
log_stages(PMEMobjpool *pop_local, enum pobj_tx_stage stage, void *arg);

unsigned int get_free_slots_left_in_the_buffer(struct ring_buffer *rb);

bool buffer_has_space_for_command(struct ring_buffer *rb, int command_lenght);


void print_available_buffer_slots(struct ring_buffer *rb);

int insert_in_to_buffer(PMEMobjpool *pop, struct ring_buffer *rb, char *command, long flush_period, int commandLength);

int log_to_pmem(PMEMobjpool *pop, struct ring_buffer *rb, char *command, long flush_period);

struct cmd_and_next_index *retrieve_command_at_index(PMEMobjpool *pop, struct ring_buffer *rb, int index);

PMEMobjpool *mmap_pmem_object_pool_ring_buffer(PMEMobjpool *pop);

struct ring_buffer * initialise_ring_buffer_on_persistent_memory(PMEMobjpool *pop);

void print_head_and_tail(struct ring_buffer *rb);

void print_commands_in_the_buffer(struct ring_buffer *rb);

struct ring_buffer *initialise_ring_buffer(PMEMobjpool *pop);

void reset_ring_buffer(PMEMobjpool *pop, struct ring_buffer *rb);

#endif //SM_WITH_STATIC_MEMORY_HEAP_ALLOCATOR_RING_BUFFER_H
