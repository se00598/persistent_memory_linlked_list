//
// Created by se00598 on 5/5/22.
//

#include <stdbool.h>
#include <unistd.h>
#include "include/common.h"

void sm_op_begin(void){}
void sm_op_end(void){}
bool file_exists(const char *path){
    return access(path, F_OK) != 0;
}