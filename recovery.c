//
// Created by se00598 on 5/5/22.
//

#include "include/recovery.h"
#include "include/heap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <libpmemobj/base.h>
#include <libpmemobj/pool_base.h>
#include <libpmemobj/types.h>
#include <assert.h>



void run_graceful_exit(){}

struct metaData *runInitProcedure(struct memoryAgnosticRoot *memoryAgnosticRoot, bool firstInit, bool do_the_mmap){


    int currentOffset = 0;
    size_t memoryBlockSize = 4096000000;
                           //  1024000000
    if(do_the_mmap){
        printf("mmaping\n");
        memoryAgnosticRoot = mmap(NULL, memoryBlockSize,PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    }

    memoryAgnosticRoot->metaData.marPtr = memoryAgnosticRoot;
    //printf("Initiated startAddr to: %p\n", memoryAgnosticRoot->memory_buff);
    memoryAgnosticRoot->metaData.startAddr = memoryAgnosticRoot->memory_buff;
    printf("Initiated startAddr to: %p\n", memoryAgnosticRoot->metaData.startAddr);
    memoryAgnosticRoot->metaData.memoryBlockSize = memoryBlockSize;
    memcpy(memoryAgnosticRoot->msg, "Success", 9);
    currentOffset+=sizeof (struct metaData);
    memoryAgnosticRoot->metaData.heap = memoryAgnosticRoot->metaData.startAddr+currentOffset;
    currentOffset+= sizeof(heap_t);

    if(firstInit){
        memset(memoryAgnosticRoot->metaData.heap, 0, sizeof(heap_t));
    }

    memoryAgnosticRoot->metaData.region = memoryAgnosticRoot->metaData.startAddr+currentOffset;
    currentOffset+=HEAP_INIT_SIZE;
    if(firstInit) {
        memset(memoryAgnosticRoot->metaData.region, 0, HEAP_INIT_SIZE);
    }

    for (int i = 0; i < BIN_COUNT; i++) {
        memoryAgnosticRoot->metaData.heap->bins[i] = memoryAgnosticRoot->metaData.startAddr+currentOffset;
        currentOffset+=sizeof(bin_t);
        if(firstInit) {
            memset(memoryAgnosticRoot->metaData.heap->bins[i], 0, sizeof(bin_t));
        }
    }
    if(firstInit) {
        init_heap(memoryAgnosticRoot->metaData.heap, (long) memoryAgnosticRoot->metaData.region);
    }
    printf("Heap ptr at th end of init %p\n", memoryAgnosticRoot->metaData.heap);
    return &memoryAgnosticRoot->metaData;
}

PMEMobjpool *mmap_pmem_object_pool(PMEMobjpool *pop){
    //char *path_to_pmem = "/mnt/dax/test_outputs/mmaped_files/allocator_file";
    char *path_to_pmem = "/mnt/dax/test_outputs/mmaped_files/app_recovery_file";
    if ((pop = pmemobj_open(path_to_pmem, POBJ_LAYOUT_NAME(list))) == NULL) {
        perror("Failed to open pool\n");
        exit(1);
    }

    return pop;
}

void *getDramRootPtr(PMEMobjpool *pop){
    printf("Getting a root\n");
    PMEMoid root = pmemobj_root(pop, sizeof(struct my_root));
    printf("got root\n");
    struct my_root *rootp = pmemobj_direct(root);
    printf("Test msg: %s\n", rootp->test_msg);
    return rootp->pmem_start;
}

struct memoryAgnosticRoot *recoverMetaData(struct memoryAgnosticRoot *dramRoot){
    PMEMobjpool *pop = mmap_pmem_object_pool(pop);
    struct memoryAgnosticRoot *memoryAgnosticRoot = getDramRootPtr(pop);
    struct metaData *tempDramMetaData = malloc(sizeof (struct metaData));
    //Copy values from pmem
    printf("Block size on pmem : %zu\n", memoryAgnosticRoot->metaData.memoryBlockSize);
    printf("Addr stored on pmem: %p\n", memoryAgnosticRoot->metaData.marPtr);
    memcpy(tempDramMetaData, &memoryAgnosticRoot->metaData, sizeof(struct metaData));
    pmemobj_close(pop);
    printf("mmaping to addr: %p\n", tempDramMetaData->marPtr);
    void *temp_ptr = mmap(tempDramMetaData->marPtr, tempDramMetaData->memoryBlockSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS , -1, 0);
    void *target_ptr = tempDramMetaData->marPtr;
    long position = target_ptr - temp_ptr;
    printf("Distance is: %zu\n", position);
    if(position > 0){
        printf("Block position is off by: %zu, fixing.\n", position);
        munmap(temp_ptr, tempDramMetaData->memoryBlockSize);
        temp_ptr = mmap(tempDramMetaData->marPtr, tempDramMetaData->memoryBlockSize+position, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS , -1, 0);
        printf("Fixed\n");
    } else if(position == 0){
        printf("Block position is correct\n");
    } else{
        printf("Block position is off, trying to fix \n");
        printf("NOT IMPLEMENTED");
        exit(1);
    }
    dramRoot = temp_ptr+position;
    printf("Address of DRAM pointer: %p\n", dramRoot);
    assert(dramRoot == tempDramMetaData->marPtr);
    printf("Post mapping");
    PMEMobjpool *pop2 = mmap_pmem_object_pool(pop2);
    memoryAgnosticRoot = getDramRootPtr(pop2);
    printf("Copying\n");
    memcpy(dramRoot, memoryAgnosticRoot, 4096000000);
    printf("Freeing\n");
    free(tempDramMetaData);
    printf("Closing\n");
    pmemobj_close(pop2);
    printf("Address of DRAM pointer: %p\n", dramRoot);
    printf("MSG from dram root: %s\n", dramRoot->msg);
    printf("Heap ptr: %p\n", dramRoot->metaData.heap);
    printf("Start address %p\n", dramRoot->metaData.startAddr);
    return dramRoot;
}